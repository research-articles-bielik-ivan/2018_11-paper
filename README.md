## About repository

The repository contains necessary files to conduct analysis of economic indicators of six post-communist countries based on the ideology of the ruling government.

The data for the analysis is stored in [OSF repository](https://osf.io/ugtqx/), file `article_data.csv`.

### Using a virtual environment

For a proper function of the Jupyter Notebook, you need to create a separated Python environment on your local machine.

On Mac/Linux (you have to be in the project directory)

```
python3 -m venv env
source env/bin/activate
pip install -r requirements.txt
```
